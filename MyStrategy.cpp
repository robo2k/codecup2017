#include "MyStrategy.h"

#define PI 3.14159265358979323846
#define PI2 (PI*2.0f)
#define _USE_MATH_DEFINES

#include <cmath>
#include <cstdlib>
#include <vector>
#include <map>
#include <deque>
#include <set>
#include <fstream>
#include <algorithm>
#include <memory>
#include <cstring>
#ifdef DEBUG
#include <string>
#endif

using namespace model;
using namespace std;

std::ofstream Log;

const char * VN(int I) {
	static const char * VcNames[] = { "ARRV","FIGHTER","HELICOPTER","IFV","TANK" };
	if (I <= -1 || I > 4 ) return "UNKNOWN";
	return VcNames[I];
}

const char * AN(int I) {
	static const char * ActionNames[] = { "NONE","CLEAR_AND_SELECT","ADD_TO_SELECTION","DESELECT","ASSIGN", "DISMISS","DISBAND","MOVE","ROTATE","SCALE","SETUP_VEHICLE_PRODUCTION","TACTICAL_NUCLEAR_STRIKE" };
	if (I <= -1 || I > 11) return "UNKNOWN";
	return ActionNames[I];
}

const char * BTS(bool b) {
	if (b) return "True"; else return "False";
}

void LogMove(const Move & M) {
	Log << AN((int)M.getAction()) << ", x: " << M.getX() << ", y: " << M.getY() << ", Vehicle: " << VN((int)M.getVehicleType()) << ", Angle: " << M.getAngle() << ", Group: " << M.getGroup()
		<< ", Factor: " << M.getFactor();
	Log << ", min (" << M.getLeft() << "," << M.getTop() << ") max (" << M.getRight() << "," << M.getBottom() << ")" << endl;
}

double sqr(double x) { return x*x; };

double normAngle(double InAngle) {
	while (InAngle > PI2) { InAngle -= PI2; }
	while (InAngle < 0.0f) { InAngle += PI2; }
	return InAngle;
}

double ToDeg(double rad) { return rad * (180.0f / PI); }
double ToRad(double ang) { return ang * (PI / 180.0f); }

struct vec2 {
	double x, y;

	inline vec2() :x(0), y(0) {};
	inline vec2(const Unit & v) :x(v.getX()),y(v.getY()) {};
	inline vec2(double _x, double _y) :x(_x), y(_y) {};
	inline vec2 operator-(vec2 v) const {return vec2(x - v.x, y - v.y);}
	inline vec2 operator+(vec2 v) const {return vec2(x + v.x, y + v.y);}
	inline vec2 operator*(double d) const {return vec2(x*d, y*d);}
	inline bool operator==(vec2 v) const { return (x == v.x && y == v.y); }
	inline double dist(vec2 in) const {
		return sqrt(sqr(in.x - x) + sqr(in.y - y));
	}
	inline double dist2(vec2 in) const {
		return sqr(in.x - x) + sqr(in.y - y);
	}
	inline double angle(vec2 in) const {
		double dot = x*in.x + y*in.y;
		double det = x*in.y - y*in.x;
		return atan2(det, dot);
	}
	inline double len2() const {return sqr(x) + sqr(y);}
	inline double len() const {return sqrt(sqr(x) + sqr(y));}
	void normalise() {
		double l = len();
		x /= l;
		y /= l;
	}
	void scale(double f) {
		normalise();
		x *= f;
		y *= f;
	}
	bool isZero() const { return x == 0 && y == 0; }
};

double GetAngleTo(vec2 To) {
	return vec2(10, 0).angle(To);
}

struct Rect {
	vec2 Min, Max;

	Rect() {};
	Rect(vec2 _min, vec2 _max) :Min(_min), Max(_max) {};
	Rect(double minx, double miny, double maxx, double maxy) :Min(minx, miny), Max(maxx, maxy) {};
	Rect(vec2 Center, double Radius) {
		Min.x = Center.x - Radius;
		Min.y = Center.y - Radius;
		Max.x = Center.x + Radius;
		Max.y = Center.y + Radius;
	}
	double Width() const { return Max.x - Min.x; };
	double Height() const { return Max.y - Min.y; };

	vec2 Center() const {
		return vec2( Min.x + Width()*0.5, Min.y + Height()*0.5);
	}

	bool Contains(vec2 P) const {
		return P.x > Min.x && P.x < Max.x && P.y > Min.y && P.y < Max.y;
	}

	bool Intersect(const Rect & r) const {
		return Contains(r.Min) || Contains(r.Max) || r.Contains(Min) || r.Contains(Max);
	}
	vec2 Clamp(vec2 v) const {
		return vec2(min(Max.x,max(Min.x, v.x)), min(Max.y,max(Min.y, v.y)));
	}

	void AddVec(const vec2 & v) {
		Min.x = min(Min.x, v.x);
		Min.y = min(Min.y, v.y);
		Max.x = max(Max.x, v.x);
		Max.y = max(Max.y, v.y);
	}

	void SetCenter(vec2 c) {

	}
};

template <typename T, typename F>
void SortVector(vector<T> & Vec, F Func = std::less) {
	std::stable_sort(Vec.begin(), Vec.end(), Func);
}

#define WorldWidth ThisGame.getWorldWidth()

vec2 Negative = { -1,-1 };
vec2 Zero = { 0,0 };
vec2 WorldMin = { 0.0f,0.0f };
vec2 WorldMax = { 1024.0f,1024.0f };
vec2 WorldCenter = { 512, 512 };


Player Me,Opponent;
World ThisWorld;
Game ThisGame;

map<int, Vehicle> PrevTick;
map<int, Vehicle> ThisTick;

#define ID(Unit) (int)Unit.getId()

struct VcDelta {
	vec2	Vec;
	int		Hp;

	VcDelta() {};
	VcDelta(const Vehicle & Vc) {
		const Vehicle & Prev = PrevTick[(int)Vc.getId()];
		Vec.x = Vc.getX() - Prev.getX();
		Vec.y = Vc.getY() - Prev.getY();
		Hp = Vc.getDurability() - Prev.getDurability();
	}
};

bool HasSelectedUnits = false;
//int SelectedGroup = 0;

int WorldTick = 0;

int NumDied = 0;
void InitUnits() {
	HasSelectedUnits = false;
	PrevTick = ThisTick;

	for (const auto & Vc : ThisWorld.getNewVehicles()) {
		ThisTick.insert(pair<int, Vehicle>(ID(Vc), Vc));
	}
	for (const auto & Upd : ThisWorld.getVehicleUpdates()) {
		if (Upd.getDurability() == 0) {
			ThisTick.erase(ID(Upd));
			NumDied++;
		}
		else {
			ThisTick[ID(Upd)] = Vehicle(ThisTick[ID(Upd)], Upd);
			HasSelectedUnits |= Upd.isSelected();
		}
	}

	if (WorldTick == 0) {
		PrevTick = ThisTick;
	}

};

void InitGlobals(const Player & _Me, const World & _World, const Game & _Game) {
	Me = _Me;
	Opponent = _World.getOpponentPlayer();
	ThisWorld = _World;
	ThisGame = _Game;
	WorldTick = ThisWorld.getTickIndex();
}

#define ANY_VEHICLE VehicleType::_UNKNOWN_
#define ANY_SELECTED -1

#define VT VehicleType
#define AT ActionType

struct VhStats {
	int GndDmg;
	int GndDef;
	int AirDmg;
	int AirDef;
	double AirRange;
	double GndRange;
	double Vision;
	double Speed;
	bool IsAerial;

	VhStats() {};
	VhStats(VehicleType Type) {
		switch (Type) {
		case VehicleType::ARRV:
			GndDmg = 0;
			GndDef = ThisGame.getArrvGroundDefence();
			AirDmg = 0;
			AirDef = ThisGame.getArrvAerialDefence();
			AirRange = 0;
			GndRange = 0;
			Vision = ThisGame.getArrvVisionRange();
			Speed = ThisGame.getArrvSpeed();
			IsAerial = false;
			break;
		case VehicleType::FIGHTER:
			GndDmg = ThisGame.getFighterGroundDamage();
			GndDef = ThisGame.getFighterGroundDefence();
			AirDmg = ThisGame.getFighterAerialDamage();
			AirDef = ThisGame.getFighterAerialDefence();
			AirRange = ThisGame.getFighterAerialAttackRange();
			GndRange = ThisGame.getFighterGroundAttackRange();
			Vision = ThisGame.getFighterVisionRange();
			Speed = ThisGame.getFighterSpeed();
			IsAerial = true;
			break;
		case VehicleType::HELICOPTER:
			GndDmg = ThisGame.getHelicopterGroundDamage();
			GndDef = ThisGame.getHelicopterGroundDefence();
			AirDmg = ThisGame.getHelicopterAerialDamage();
			AirDef = ThisGame.getHelicopterAerialDefence();
			AirRange = ThisGame.getHelicopterAerialAttackRange();
			GndRange = ThisGame.getHelicopterGroundAttackRange();
			Vision = ThisGame.getHelicopterVisionRange();
			Speed = ThisGame.getHelicopterSpeed();
			IsAerial = true;
			break;
		case VehicleType::IFV:
			GndDmg = ThisGame.getIfvGroundDamage();
			GndDef = ThisGame.getIfvGroundDefence();
			AirDmg = ThisGame.getIfvAerialDamage();
			AirDef = ThisGame.getIfvAerialDefence();
			AirRange = ThisGame.getIfvAerialAttackRange();
			GndRange = ThisGame.getIfvGroundAttackRange();
			Vision = ThisGame.getIfvVisionRange();
			Speed = ThisGame.getIfvSpeed();
			IsAerial = false;
			break;
		case VehicleType::TANK:
			GndDmg = ThisGame.getTankGroundDamage();
			GndDef = ThisGame.getTankGroundDefence();
			AirDmg = ThisGame.getTankAerialDamage();
			AirDef = ThisGame.getTankAerialDefence();
			AirRange = ThisGame.getTankAerialAttackRange();
			GndRange = ThisGame.getTankGroundAttackRange();
			Vision = ThisGame.getTankVisionRange();
			Speed = ThisGame.getTankSpeed();
			IsAerial = false;
			break;
		default:
			GndDmg = 0;
			GndDef = 0;
			AirDef = 0;
			AirDmg = 0;
			AirRange = 0;
			GndRange = 0;
			Vision = 0;
			Speed = 0;
			IsAerial = false;
		}
	}

	//static vector<VehicleType> GetTypesByTargetPrio(VehicleType t) {
	//	VhStats(t);
	//	vector<VhStats> a;
	//	for (int i = 0; i < 5; i++) {
	//		a.push_back(VhStats((VT)i);
	//	}
	//}

};

enum Faction {
	FACTION_ALL,
	FACTION_ENEMY,
	FACTION_ALLY
};

enum PresenceType {
	PRESENCE_GND,
	PRESENCE_AIR,
	PRESENCE_MIX
};

bool IsInGroup(const Vehicle & V, int g) {
	if (g == 0 && V.getPlayerId() == Me.getId()) return true;
	if (g == ANY_SELECTED && V.isSelected()) return true;
	if (V.getGroups().empty()) return false;
	for (auto Gp : V.getGroups()) {
		if (Gp == g) return true;
	}
	return false;
}

bool IsType(const Vehicle & V, VehicleType Type) {
	if (Type == VT::_UNKNOWN_) return true;
	if (V.getType() == Type) return true;
	return false;
}

bool IsMoveType(const Vehicle & v, PresenceType m) {
	if (m == PRESENCE_MIX) return true;
	if (m == PRESENCE_AIR && v.isAerial()) return true;
	if (m == PRESENCE_GND && !v.isAerial()) return true;
	return false;
}

bool IsFaction(const Vehicle & V, Faction InFaction) {
	if (V.getPlayerId() != Me.getId() && InFaction == FACTION_ALLY) { return false; }
	if (V.getPlayerId() == Me.getId() && InFaction == FACTION_ENEMY) { return false; }
	return true;
}

void GetCellAt(const vec2 & u, int & x, int & y, int NumCells) {
	double CellWidth = WorldWidth / NumCells;
	x = (int)ceil(u.x / CellWidth) - 1;
	y = (int)ceil(u.y / CellWidth) - 1;
}
Rect GetFacilityRect(const Facility & F) {
	return Rect(F.getLeft(), F.getTop(), F.getLeft() + 64, F.getTop() + 64);
}

Facility GetFacilityAt(vec2 Point) {
	for (const Facility & Fac : ThisWorld.getFacilities()) {
		if (GetFacilityRect(Fac).Contains(Point)) {
			return Fac;
		}
	}
	return Facility();
}

WeatherType GetWeatherAt(vec2 v) {
	int cx, cy;
	GetCellAt(v, cx, cy, ThisGame.getTerrainWeatherMapRowCount());
	return ThisWorld.getWeatherByCellXY()[cx][cy];
}

TerrainType GetTerrainAt(vec2 v) {
	int cx, cy;
	GetCellAt(v, cx, cy, ThisGame.getTerrainWeatherMapRowCount());
	return ThisWorld.getTerrainByCellXY()[cx][cy];
}

double GetVisionAt(const Vehicle & v) {
	if (v.isAerial()) {
		WeatherType t = GetWeatherAt(v);
		double f[3] = { ThisGame.getClearWeatherVisionFactor() ,ThisGame.getCloudWeatherVisionFactor() , ThisGame.getRainWeatherVisionFactor() };
		return v.getVisionRange() * f[(int)t];
	}
	else {
		TerrainType t = GetTerrainAt(v);
		double f[3] = { ThisGame.getPlainTerrainVisionFactor() ,ThisGame.getSwampTerrainVisionFactor() , ThisGame.getForestTerrainVisionFactor() };
		return v.getVisionRange() * f[(int)t];
	}
}

struct GroupInfo {
	int				NumUnits;
	int				NumType[5];

	vec2			Center;
	double			Radius;
	Rect			Bounds;

	int				Health;
	int				AvgHealth;
	int				MinHealth;
	int				MaxHealth;
	int				HealthDelta;

	double			MinSpeed;
	bool			IsSlowed;

	PresenceType		Presence;

	bool			HasMoved;
	bool			HasDamaged;
	bool			IsBlocked;

	bool			IsSelected;
	int				Types;

	vector<int>		Groups;

	int				LastUpdated;


	void Clear() {
		NumUnits = 0;
		NumType[0] = 0;
		NumType[1] = 0;
		NumType[2] = 0;
		NumType[3] = 0;
		NumType[4] = 0;
		Radius = 0.0f;
		Bounds.Min = WorldMax;
		Bounds.Max = WorldMin;
		Center = Zero;
		Health = 0;
		AvgHealth = 0;
		MinHealth = 0;
		MaxHealth = 0;
		HealthDelta = 0;
		MinSpeed = 0;
		IsSlowed = false;
		Presence = PRESENCE_MIX;
		HasMoved = false;
		HasDamaged = false;
		IsSelected = false;
		IsBlocked = false;

		Groups.clear();

		Types = 0;

		LastUpdated = -1;
	}
	GroupInfo() {
		Clear();
	}
	GroupInfo(const vector<Vehicle> & InVehicles) {
		Update(InVehicles);
	}
	bool HasType(VehicleType InType) const {
		return Types & (1 << (int)InType);
	}
	bool HasGroup(int g) const {
		for (int ng : Groups) {
			if (ng = g) return true;
		}
		return false;
	}
	double GetStrRatio(const GroupInfo & Inf) {
		//if (!Inf.NumUnits || !NumUnits) {
		//	return 0;
		//}
		double Ratio1 = 0;
		double Ratio2 = 0;

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (NumType[i] == 0 || Inf.NumType[j] == 0) continue;

				VhStats Stat1((VT)i);
				VhStats Stat2((VT)j);

				if (Stat1.IsAerial && Stat2.IsAerial) {
					Ratio1 += max(NumType[i] * Stat1.AirDmg - Inf.NumType[j] * Stat2.AirDef,0);
					Ratio2 += max(Inf.NumType[j] * Stat2.AirDmg - NumType[i] * Stat1.AirDef,0);
				}
				if (Stat1.IsAerial && !Stat2.IsAerial) {
					Ratio1 += max(NumType[i] * Stat1.GndDmg - Inf.NumType[j] * Stat2.AirDef,0);
					Ratio2 += max(Inf.NumType[j] * Stat2.AirDmg - NumType[i] * Stat1.GndDef,0);
				}
				if (!Stat1.IsAerial && Stat2.IsAerial) {
					Ratio1 += max(NumType[i] * Stat1.AirDmg - Inf.NumType[j] * Stat2.GndDef,0);
					Ratio2 += max(Inf.NumType[j] * Stat2.GndDmg - NumType[i] * Stat1.AirDef,0);
				}
				if (!Stat1.IsAerial && !Stat2.IsAerial) {
					Ratio1 += max(NumType[i] * Stat1.GndDmg - Inf.NumType[j] * Stat2.GndDef,0);
					Ratio2 += max(Inf.NumType[j] * Stat2.GndDmg - NumType[i] * Stat1.GndDef,0);
				}
			}
		}

		//if (Ratio1 == 0 || Ratio2 == 0) return 0;
		if (Ratio2 == 0) {
			return 999;
		}
		return (Ratio1 / Ratio2) * ((double)Health / (double)Inf.Health);
	}

	void PrintLog() {
		Log << "	Num units: " << NumUnits << endl;
		Log << "	Center: x:" << Center.x << " y: " << Center.y << " Radius: " << Radius << endl;
		Log << "	Width: " << Bounds.Width() << " Height: " << Bounds.Height() << endl;
		Log << "	Health: " << Health << " Avg: " << AvgHealth << " Min: " << MinHealth << " Max: " << MaxHealth << endl;
		Log << "	MinSpeed: " << MinSpeed << " Slowed: " << BTS(IsSlowed) << endl;
		Log << "	HasMoved: " << BTS(HasMoved) << endl;
		Log << "	IsBlocked: " << BTS(IsBlocked) << endl;
		Log << "	HasDamaged: " << BTS(HasDamaged) << endl;
		Log << "	IsSelected: " << BTS(IsSelected) << endl;
		for (int i = 0; i < (int)VehicleType::_COUNT_; i++) {
			Log << "		Has " << VN(i) << ": " << BTS(HasType((VehicleType)i)) << " Num: " << NumType[i] << endl;
		}
		//Log << "	LastUpdated: " << LastUpdated << endl;
	}

	GroupInfo & Update(const vector<Vehicle> & Units) {
		Clear();
		NumUnits = Units.size();

		if (!NumUnits) {
			return *this;
		}


		IsSelected = true;
		MinHealth = 100;
		MaxHealth = 0;

		MinSpeed = 1000;

		Bounds.Min = Units[0];
		Bounds.Max = Units[0];

		HasMoved = false;
		HasDamaged = false;

		bool IsAerial = true;
		bool IsGround = true;

		Types = 0;

		int NumMoved = 0;

		for (auto Vc : Units) {
			Bounds.AddVec(Vc);

			Center.x += Vc.getX();
			Center.y += Vc.getY();

			Health += Vc.getDurability();

			MinHealth = std::min(MinHealth, Vc.getDurability());
			MaxHealth = std::max(MaxHealth, Vc.getDurability());

			double Speed = Vc.getMaxSpeed();

			if (Vc.isAerial()) {
				WeatherType Weather = GetWeatherAt(Vc);
				if (Weather == WeatherType::CLOUD) {
					Speed *= ThisGame.getCloudWeatherSpeedFactor();
					IsSlowed = true;
				}
				if (Weather == WeatherType::RAIN) {
					Speed *= ThisGame.getRainWeatherSpeedFactor();
					IsSlowed = true;
				}
			}
			else {
				TerrainType Terrain = GetTerrainAt(Vc);
				if (Terrain == TerrainType::FOREST) {
					Speed *= ThisGame.getForestTerrainSpeedFactor();
					IsSlowed = true;
				}
				if (Terrain == TerrainType::SWAMP) {
					Speed *= ThisGame.getSwampTerrainSpeedFactor();
					IsSlowed = true;
				}
			}

			MinSpeed = std::min(MinSpeed, Speed);

			IsAerial &= Vc.isAerial();
			IsGround &= !Vc.isAerial();

			Types |= (1 << (int)Vc.getType());

			NumType[(int)Vc.getType()]++;

			IsSelected &= Vc.isSelected();

			for (int ng : Vc.getGroups()) {
				if (!HasGroup(ng)) {
					Groups.push_back(ng);
				}
			}

			VcDelta Delta(Vc);

			HealthDelta += Delta.Hp;

			if (!Delta.Vec.isZero()) NumMoved++;
			if (Delta.Hp < 0) HasDamaged = true;

		}

		if (NumMoved > 0) {
			HasMoved = true;
		}
		if (NumMoved < NumUnits && HasMoved) {
			IsBlocked = true;
		}

		if (IsAerial && !IsGround) {
			Presence = PRESENCE_AIR;
		}
		if (!IsAerial && IsGround) {
			Presence = PRESENCE_GND;
		}
		if (!IsAerial && !IsGround) {
			Presence = PRESENCE_MIX;
		}

		Radius = std::max(Bounds.Width()*0.5, Bounds.Height() *0.5);

		Center.x /= NumUnits;
		Center.y /= NumUnits;

		AvgHealth = (int)(Health / NumUnits);

		LastUpdated = WorldTick;

		return *this;
	}
};

struct fgroup { int Grp; fgroup(int _grp) :Grp(_grp) {}; inline bool operator()(const Vehicle & V) const { return IsInGroup(V, Grp); } };
struct fngroup { int Grp; fngroup(int _grp) :Grp(_grp) {}; inline bool operator()(const Vehicle & V) const { return !IsInGroup(V, Grp); } };
struct ftype { VehicleType t; Faction ft; ftype(VehicleType _t) :t(_t) {}; inline bool operator()(const Vehicle & V) const { return IsType(V, t); } };
struct ffaction {Faction ft;ffaction(Faction _ft) :ft(_ft) {}; inline bool operator()(const Vehicle & V) const { return IsFaction(V,ft); }};
struct fpresence {PresenceType mt;fpresence(PresenceType _mt) :mt(_mt) {};inline bool operator()(const Vehicle & V) const { return IsMoveType(V, mt); }};
struct frect {Rect r;frect(Rect _r) { r = _r; }inline bool operator()(const Vehicle & V) const {return r.Contains(V);}};
struct fradius {vec2 c;double r;fradius(vec2 _c, double _r) { c = _c; r = _r; };inline bool operator()(const Vehicle & V) const { return (c.dist2(V) < sqr(r)); }};
struct fhpmore {int Health;fhpmore(int _d) :Health(_d) {};inline bool operator()(const Vehicle & V) const { return V.getDurability() > Health; }};
bool femptygp(const Vehicle & Vc) { return Vc.getGroups().empty(); };

class VehicleGp {
	mutable GroupInfo	Inf;
public:
	vector<Vehicle>	V;

	VehicleGp() {}
	VehicleGp(const vector<Vehicle> & InV) :V(InV) {};
	template <typename F> 
	VehicleGp(const vector<Vehicle> & InV, F Filter) {
		V.reserve(InV.size()); for (auto const & Vc : InV) {if (Filter(Vc)) V.push_back(Vc);}
	}
	GroupInfo GetInfo() const {
		if (Inf.LastUpdated == WorldTick) return Inf;
		Inf.Update(V);
		return Inf;
	}
	VehicleGp FilterGroup(int g) const {return VehicleGp(V,fgroup(g));}
	VehicleGp FilterNotGroup(int g) const { return VehicleGp(V, fngroup(g)); }
	VehicleGp FilterEmptyGroup() const { return VehicleGp(V, femptygp); }
	VehicleGp FilterType(VehicleType vt) const {return VehicleGp(V,ftype(vt));}
	VehicleGp FilterFaction(Faction ft) const { return VehicleGp(V, ffaction(ft)); }
	VehicleGp FilterRect(Rect r) const {return VehicleGp(V,frect(r));}
	VehicleGp FilterRadius(vec2 c, double r) const {return VehicleGp(V,fradius(c, r));}
	VehicleGp FilterPresence(PresenceType mt) const { return VehicleGp(V, fpresence(mt)); }
	VehicleGp FilterMoreHP(int hp) const { return VehicleGp(V, fhpmore(hp)); }

	bool IsEmpty() const {
		return V.empty();
	}

	void SortByDurability() {
		SortVector(V, [](const Vehicle & V1, const Vehicle & V2) { return V1.getDurability() > V2.getDurability();  });
	}

	static VehicleGp GetSelected() {
		return GetAll().FilterGroup(ANY_SELECTED);
	}

	static VehicleGp GetAll() {
		static VehicleGp All;
		int Updated = -1;
		if (Updated == WorldTick) {
			return All;
		}
		All.V.clear();
		All.V.reserve(ThisTick.size());
		for (auto V : ThisTick) {
			All.V.push_back(V.second);
		}
		Updated = WorldTick;
		return All;
	}
};

bool RectOccupied(Rect r, PresenceType mt, Faction f = FACTION_ALL) {
	return !VehicleGp::GetAll().FilterRect(r).FilterPresence(mt).FilterFaction(f).V.empty();
}

char P(int N) { if (N) return '#';  else  return'0'; }
#define STARTING_DIST 74
#define P1 45
#define P2 119
#define P3 193
#define TOLERANCE 0.5
struct imap {
	int c[9];
	imap() { std::memset(c, 0, sizeof(c)); };
	imap(const int _c[9]) { std::memcpy(c, _c, sizeof(c)); };
	bool operator==(const imap & m2) const { return memcmp(c, m2.c, sizeof(c)) == 0; };
	int operator[](int i) const { return c[i]; };
	int & operator[](int i) { return c[i]; };
	static void GetLine(int ind, int & n1, int & n2, int & n3) {
		switch (ind) {
			case 1:	n1 = 0;	n2 = 1;	n3 = 2;	return;
			case 2:	n1 = 3;	n2 = 4;	n3 = 5;	return;
			case 3:	n1 = 6;	n2 = 7;	n3 = 8;	return;
			case 4:	n1 = 0;	n2 = 3;	n3 = 6;	return;
			case 5:	n1 = 1;	n2 = 4;	n3 = 7;	return;
			case 6:	n1 = 2;	n2 = 5;	n3 = 8;	return;
		}
	}
	void PrintLog() const {
		Log << P(c[0]) << P(c[1]) << P(c[2]) << endl;
		Log << P(c[3]) << P(c[4]) << P(c[5]) << endl;
		Log << P(c[6]) << P(c[7]) << P(c[8]) << endl;
	}
	static int ipos(vec2 p) {
		if (p.x == P1 && p.y == P1) return 0;
		if (p.x == P2 && p.y == P1) return 1;
		if (p.x == P3 && p.y == P1) return 2;
		if (p.x == P1 && p.y == P2) return 3;
		if (p.x == P2 && p.y == P2) return 4;
		if (p.x == P3 && p.y == P2) return 5;
		if (p.x == P1 && p.y == P3) return 6;
		if (p.x == P2 && p.y == P3) return 7;
		if (p.x == P3 && p.y == P3) return 8;
		return -1;
	}
	static int InitPos(VehicleType t) {
		return ipos(VehicleGp::GetAll().FilterType(t).FilterFaction(FACTION_ALLY).GetInfo().Center);
	}
	void AddType(VehicleType Type) {
		c[InitPos(Type)] = (int)Type+1;
	}
};
typedef int(*icmp)(const imap & m1);
struct imove {
	int c1, c2, g;
	imap m;
	imove(int _c1, int _c2, int _g) :c1(_c1), c2(_c2), g(_g) {};
};
#define PushMove(a1,a2) Moves.push_back( imove(a1, a2, Sp[a1]) );Res = RecursiveBuild(Sp, Turns - 1, Cmp, Moves); if( !Res ) { Moves.pop_back(); } else { return Res; }
int RecursiveBuild(imap Sp, int Turns, icmp Cmp, vector<imove> & Moves) {
	int N = Moves.size();
	int This = 0;
	int Prev = 0;
	if (N) {
		This = Moves[N - 1].c2;
		Prev = Moves[N - 1].c1;
		if (Sp[This]) {
			return 0;
		}
		if (N >= 2) {
			if (This == Moves[N - 2].c1 && Prev == Moves[N - 2].c2) {
				return 0;
			}
		}
		Sp[This] = Sp[Prev];
		Sp[Prev] = 0;
		Moves[N - 1].m = Sp;
	}

	int Res = Cmp(Sp);

	if (Res) return Res;

	if (Turns == 0) return 0;

	if (Sp[0]) { PushMove(0, 1); PushMove(0, 3); }
	if (Sp[1]) { PushMove(1, 0); PushMove(1, 4); PushMove(1, 2); }
	if (Sp[2]) { PushMove(2, 1); PushMove(2, 5); }
	if (Sp[3]) { PushMove(3, 0); PushMove(3, 4); PushMove(3, 6); }
	if (Sp[4]) { PushMove(4, 1); PushMove(4, 5); PushMove(4, 7); PushMove(4, 3); }
	if (Sp[5]) { PushMove(5, 2); PushMove(5, 8); PushMove(5, 4); }
	if (Sp[6]) { PushMove(6, 3); PushMove(6, 7); }
	if (Sp[7]) { PushMove(7, 4); PushMove(7, 8); PushMove(7, 6); }
	if (Sp[8]) { PushMove(8, 5); PushMove(8, 7); }

	return 0;
}
int RecursiveBuild(imap Sp, icmp Cmp, vector<imove> & Moves) {
	Moves.clear();
	int N = 2;
	int Res = 0;
	while (!(Res = RecursiveBuild(Sp, N, Cmp, Moves))) N++;
	return Res;
}
#undef PushMove

struct DMove {
	Move	M;
	int		Expire;
	bool	UntilStop;
	bool	Local;
	bool	CanSwitch;

#ifdef DEBUG
	string Comment;
#endif

	void Clear() {
		Expire = 0;
		UntilStop = false;
		Local = false;
		CanSwitch = false;
	}
	void SetAction(ActionType A) {
		M.setAction(A);
	}
	void SetVehicleType(VehicleType V) {
		M.setVehicleType(V);
	}
	void SetGroup(int Ind) {
		M.setGroup(Ind);
	}
	void SetRect(vec2 Min, vec2 Max) {
		M.setLeft(Min.x);
		M.setTop(Min.y);
		M.setRight(Max.x);
		M.setBottom(Max.y);
	}
	void SetVec(vec2 V) {
		M.setX(V.x);
		M.setY(V.y);
	}
	vec2 GetVec() const {
		return vec2(M.getX(), M.getY());
	}
	vec2 GetMin() const {
		return vec2(M.getLeft(), M.getTop());
	}
	vec2 GetMax() const {
		return vec2(M.getRight(), M.getBottom());
	}
	void PrintLog() {
		LogMove(M);
		Log << "	Expires: " << Expire << endl;
		Log << "	UntilStop: " << BTS(UntilStop) << endl;
#ifdef DEBUG
		Log << "	Comment: " << Comment << endl;
#endif
	}
	DMove() {
		Clear();
	}
};

#define MAX_SQUADS 30

class Squad;
deque<Squad*> Squads;
class Squad {
protected:
	int				Index = 0;

	GroupInfo		Info;

	DMove			ThisMove;
	deque<DMove>	Moves;

	int				NumFinished = 0;

	int				MoveNum = 0; //dbg

	int				NextThink = 0;

	static map<int, DMove> GroupMove;

public:

	Squad() {
	}

	void Comment(const char * c) {
#ifdef DEBUG
		if (Moves.empty()) return;
		Moves.back().Comment = c;
#endif
	}
	void PopMove() {
		Moves.pop_back();
	}
	void PushMove(DMove Dm) {
		Moves.push_back(Dm);
	}
	void Assign(int Group) {
		DMove Dm;
		Dm.SetAction(ActionType::ASSIGN);
		Dm.SetGroup(Group);
		Dm.Expire = 0;
		Dm.UntilStop = false;

		PushMove(Dm);
	}
	void SelectGroup(int Group) {
		DMove Dm;
		Dm.SetAction(ActionType::CLEAR_AND_SELECT);
		Dm.SetGroup(Group);
		Dm.Expire = 0;

		PushMove(Dm);
	}
	void ClearSelect(VehicleType Type) {
		DMove Dm;
		Dm.SetAction(ActionType::CLEAR_AND_SELECT);
		Dm.SetVehicleType(Type);
		Dm.SetRect(WorldMin, WorldMax);
		Dm.Expire = 0;

		PushMove(Dm);
	}
	void ClearSelect(vec2 Min, vec2 Max ) {
		DMove Dm;
		Dm.SetAction(ActionType::CLEAR_AND_SELECT);
		Dm.SetRect(Min, Max);
		Dm.Expire = 0;

		PushMove(Dm);
	}
	void ClearSelect(Rect r) {
		ClearSelect(r.Min, r.Max);
	}
	void SelectLocal(Rect r, VehicleType vt = VT::_UNKNOWN_) {
		DMove Dm;
		Dm.SetAction(ActionType::CLEAR_AND_SELECT);
		Dm.SetVehicleType(vt);
		Dm.SetRect(r.Min, r.Max);
		Dm.Expire = 0;
		Dm.Local = true;

		PushMove(Dm);
	}
	void AddSelect(Rect r) {
		DMove Dm;
		Dm.SetAction(ActionType::ADD_TO_SELECTION);

		PushMove(Dm);
	}
	void AddSelect(int Group) {

	}
	void MoveTo(vec2 Vec, double MaxSpeed = 0) {
		DMove Dm;
		Dm.UntilStop = true;
		Dm.SetAction(ActionType::MOVE);
		Dm.SetVec(Vec);
		Dm.M.setMaxSpeed(MaxSpeed);
		Dm.CanSwitch = true;
		
		PushMove(Dm);
	}
	void MoveAbs(vec2 Vec) {
		DMove Dm;
		Dm.UntilStop = true;
		Dm.SetAction(ActionType::MOVE);
		Dm.SetVec(Vec);
		Dm.Local = true;
		Dm.CanSwitch = true;

		PushMove(Dm);
	}
	void StopMove() {
		MoveTo(vec2(0, 0));
	}
	void Scale(double Factor, vec2 Pivot = Zero) {
		DMove Dm;
		Dm.SetAction(ActionType::SCALE);
		Dm.SetVec(Pivot);
		Dm.M.setFactor(Factor);
		Dm.UntilStop = true;
		Dm.Local = true;
		Dm.CanSwitch = true;

		PushMove(Dm);
	}
	void ScaleAbs(double Factor, vec2 Pivot ) {
		DMove Dm;
		Dm.SetAction(ActionType::SCALE);
		Dm.SetVec(Pivot);
		Dm.M.setFactor(Factor);
		Dm.UntilStop = true;
		Dm.Local = false;
		Dm.CanSwitch = true;
		PushMove(Dm);
	}
	void Rotate(double Angle, double Limit) {
		DMove Dm;
		Dm.SetAction(ActionType::ROTATE);
		Dm.M.setAngle(Angle);
		Dm.M.setMaxAngularSpeed(Limit);
		Dm.UntilStop = true;
		Dm.Local = true;
		Dm.CanSwitch = true;
		PushMove(Dm);
	}
	void Wait(int Ticks) {
		DMove Dm;
		Dm.SetAction(ActionType::NONE);
		Dm.Expire = Ticks;
		Dm.CanSwitch = true;

		PushMove(Dm);
	}
	void WaitUntilStop() {
		DMove Dm;
		Dm.SetAction(ActionType::NONE);
		Dm.Expire = 10;
		Dm.UntilStop = true;
		Dm.CanSwitch = true;

		PushMove(Dm);
	}
	void WaitForGroup(int Index) {
		SelectGroup(Index);
		WaitUntilStop();
	}
	void WaitForSpace(Rect MinMax) {

	}
	void Disband() {
		DMove Dm;
		Dm.SetAction(ActionType::DISBAND);
		Dm.Expire = 0;
		Dm.UntilStop = false;

		PushMove(Dm);
	}
	void Dismiss( int Group ) {
		DMove Dm;
		Dm.SetAction(ActionType::DISMISS);
		Dm.Expire = 0;
		Dm.UntilStop = false;
		Dm.SetGroup(Group);

		PushMove(Dm);
	}
	void Deselect( int Group ) {
		DMove Dm;
		Dm.SetAction(ActionType::DESELECT);
		Dm.Expire = 0;
		Dm.UntilStop = false;
		Dm.SetGroup(Group);
		PushMove(Dm);
	}
	void TacticalNuclearStike(vec2 Pos, int VehicleId) {
		DMove Dm;
		Dm.SetAction(ActionType::TACTICAL_NUCLEAR_STRIKE);
		Dm.M.setVehicleId(VehicleId);
		Dm.SetVec(Pos);
		Dm.Expire = 35;
		Dm.UntilStop = false;
		Dm.CanSwitch = true;
		PushMove(Dm);
	}
	void SetupProduction(VehicleType Type, int Id) {
		DMove Dm;
		Dm.SetAction(AT::SETUP_VEHICLE_PRODUCTION);
		Dm.M.setFacilityId(Id);
		Dm.SetVehicleType(Type);
		Dm.CanSwitch = true;
		PushMove(Dm);
	}
	void NextMove() {
		if (!Moves.empty()) {
			Moves.back().Expire = 0;
			Moves.back().UntilStop = false;
		}
	}
	void DropCommands() {
		Moves.clear();
		ThisMove.UntilStop = false;
		ThisMove.Expire = WorldTick;
	}
	void SetMaxSpeed(double Limit) {
		Moves.back().M.setMaxSpeed(Limit);
	}
	void PrintMoves() {
		Log << "DMoves: " << Moves.size() << endl;
		int i = 0;
		for (auto Dm : Moves) {
			Log << "Move #" << i << endl;
			i++;
			Dm.PrintLog();
		}
	}

	void PrintLog() {
		Log << endl;
		Log << "Squad Log " << endl;
		Log << "	Move #" << MoveNum << endl;
		Log << "	Ready: " << BTS(IsReady()) << endl;
		Log << "	Finished: " << BTS(IsFinished()) << endl;
		Log << "	Expire: " << ThisMove.Expire << " (" << WorldTick << ")" << endl;
		Log << "	Move until stop: " << BTS(ThisMove.UntilStop) << endl;
		if (Info.NumUnits) Info.PrintLog();
		ThisMove.PrintLog();
	}

	void UpdateInfo() {
		if (Info.LastUpdated == WorldTick) return;
		//int Ind = ThisMove.M.getGroup();
		//if (!Ind) Ind = -1;
		//if (!Ind) Ind = Index;
		int Ind = Index;
		if (Ind == 0) Ind = -1;
		Info = VehicleGp::GetAll().FilterGroup(Ind).GetInfo();
	}

	void SetNextThink(int Ticks) {
		if (Ticks == -1) {
			NextThink = -1;
			return;
		}
		if (Ticks == 0) {
			NextThink = 0;
		}
		else {
			NextThink = WorldTick + Ticks;
		}
	}

	bool HasMoved() const {
		//return WorldTick - LastMoved < 10;
		return Info.HasMoved;
	}

	bool IsReady() const {
		bool Res = true;
		Res &= !(ThisMove.UntilStop && HasMoved());
		Res &= (ThisMove.Expire <= WorldTick);
		return Res;
	}

	bool IsFinished() const {
		return IsReady() && Moves.empty();
	}

	virtual void Think() {};

	int GetIndex() const {
		return Index;
	}
	
	bool ShouldThink() const {
		if (NextThink == -1) return false;
		if (NextThink == 0 && !IsFinished()) return false;
		if (NextThink > WorldTick) return false;
		return true;
	}

	bool CanSwitch() const {
		bool Res = true;
		Res &= ThisMove.CanSwitch;
		Res &= (Index != 0);
		Res |= IsFinished();
		return Res;
	}

	virtual void SaveState() {

	}
	virtual void LoadState() {

	}

	bool Exec() {
		UpdateInfo();

		if (ShouldThink()) {
			DropCommands();
			NextThink = 0;
			Think();
		}

		//PrintLog();

		if (Moves.empty() || !IsReady()) {
			return false;
		}

		//if (!IsFinished()) return false;

		ThisMove = Moves.front();
		ThisMove.Expire += WorldTick;
		Moves.pop_front();

		ActionType Act = ThisMove.M.getAction();

		if (Act == AT::CLEAR_AND_SELECT) {
			Index = ThisMove.M.getGroup();
		}
		if (Act == AT::ASSIGN) {
			Index = ThisMove.M.getGroup();
		}
		if (Act == AT::DISBAND) {
		}

		MoveNum++; //dbg

		if (ThisMove.Local) {
			ThisMove.SetVec(Info.Center + ThisMove.GetVec());
			//if(ThisMove.GetMin() != Zero || )
			ThisMove.SetRect(ThisMove.GetMin() + Info.Center, ThisMove.GetMax() + Info.Center);
		}

		return true;
	}

	virtual Move GetMove() {
		if (Exec()) {
			return ThisMove.M;
		}
		return Move();
	}

	static void AddSquad(Squad * Sq) {
		Squads.push_back(Sq);
	}

	static Move GetNextMove() {
		static int SelectedGroup = 0;
		static bool Switched = false;

		Log << "Squads: " << Squads.size() << endl;

		if (Squads.empty()) return Move();

		if (Squads.size() == 1) {
			return Squads.front()->GetMove();
		}

		Move m;

		Squad * First;
		int Index;
		First = Squads.front();
		Index = First->GetIndex();
		First->UpdateInfo();

		//if (!First->IsReady()) return Move();

		if (Switched && Index != 0 /*&& First->IsReady()*/) {
			m.setAction(AT::CLEAR_AND_SELECT);
			m.setGroup(Index);
			Switched = false;
			//Log << "Switching group " << Index << endl;
		}
		else {
			//Log << "Should think: " << BTS(First->ShouldThink()) << endl;
			m = First->GetMove();
			//if (m.getAction() == AT::_UNKNOWN_) return m;
			//if (m.getAction() == AT::NONE) return m;
			if (First->CanSwitch()) {
				Squads.push_back(First);
				Squads.pop_front();
				Switched = true;
			}
			//Log << "Fetching move" << endl;
			//LogMove(m);
		}

		if (m.getAction() == AT::CLEAR_AND_SELECT) {
			SelectedGroup = m.getGroup();
		}
		return m;
	}

};

#define ARRVGP			1
#define FIGHTERGP		2
#define COPTERGP		3
#define IFVGP			4
#define TANKGP			5
#define GNDGP			6
#define AIRGP			7
#define ALLGP			8
#define TEMPGP			10
#define GPSPAWNED		40
#define GPSELECTED		99

struct SortByDist {
	SortByDist(vec2 _c) :c(_c) {};
	vec2 c;
	bool operator()(const GroupInfo & inf1, const GroupInfo inf2) const {
		return c.dist(inf1.Center) < c.dist(inf2.Center);
	}
	bool operator()(const Unit & V1, const Unit & V2) const {
		return c.dist(V1) < c.dist(V2);
	}
	bool operator()(const Facility &F1, const Facility & F2) const {
		return c.dist(vec2(F1.getLeft() + 32, F1.getTop() + 32)) < c.dist(vec2(F2.getLeft() + 32, F2.getTop() + 32));
	}
	bool operator()(const VehicleGp & G1, const VehicleGp & G2) const {
		return c.dist(G1.GetInfo().Center) < c.dist(G2.GetInfo().Center);
	}
};

struct UnitMap {
	struct Cell {
		vector<int> Indexes;
		int Cluster = 0;
		bool Visited = false;
		int NumUnits = 0;
		int Health = 0;
		inline bool IsEmpty() const { return NumUnits == 0; }
		inline bool IsProcessed() const {return Visited || Cluster != 0;}
	};

	struct Cluster {
		VehicleGp Gp;
		GroupInfo Inf;
		vector<int> Cells;
	};

	int NumCells = 0;
	int NumClusters = 0;

	vector<Cell> Cells;
	vector<Cluster> Clusters;

	UnitMap() {}
	UnitMap(int Num, const VehicleGp & All) {Init(Num, All);}

	inline bool IsInRange(int n) const {return n >= 0 && n < Cells.size();}

	void RecursiveCluster(int n, int ind, const VehicleGp & All, Cluster & This) {
		if (!IsInRange(ind)) return;

		Cell & C = Cells[ind];

		if (C.IsEmpty() || C.Visited) return;

		C.Visited = true;
		C.Cluster = n;

		for (int index : C.Indexes) {
			const Vehicle & Vh = All.V[index];
			This.Gp.V.push_back(Vh);
		}

		This.Cells.push_back(ind);

		RecursiveCluster(n, ind - 1,		All, This);
		RecursiveCluster(n, ind + 1,		All, This);
		RecursiveCluster(n, ind - NumCells, All, This);
		RecursiveCluster(n, ind + NumCells, All, This);
	}
	void Init(int Num, const VehicleGp & InGroup) {
		NumCells = Num;
		Cells.resize(NumCells*NumCells);

		int n = 0;
		for (const auto & Vc : InGroup.V) {
			int x = -1, y = -1;
			GetCellAt(Vc, x, y, NumCells);
			int ind = y*NumCells + x;
			if (ind > Cells.size() - 1) {
				Log << "Error" << endl;
			}
			Cells[ind].NumUnits++;
			Cells[ind].Indexes.push_back(n);
			Cells[ind].Health += Vc.getDurability();
			n++;
		}

		for (int i = 0; i < Cells.size(); i++) {
			if (Cells[i].IsEmpty() || Cells[i].Cluster != 0) continue;
			NumClusters++;
			Clusters.resize(NumClusters);
			Cluster & c = Clusters.back();
			RecursiveCluster(NumClusters, i, InGroup, c);
			c.Inf.Update(c.Gp.V);
		}
	}
	struct SortCluster {
		vec2 c;
		SortCluster(vec2 _c) {c = _c;}
		bool operator()(const UnitMap::Cluster & C1, const UnitMap::Cluster & C2) const {
			return c.dist(C1.Gp.GetInfo().Center) < c.dist(C2.Gp.GetInfo().Center);
		}
	};
	inline void SortByDist(vec2 Point) {
		SortVector(Clusters, SortCluster(Point));
	}

	Rect GetCell(int n) const {
		int y = n / NumCells;
		int x = n % NumCells;
		double w = 1024.0 / (double)NumCells;
		return Rect(x*w,y*w,(x+1)*w,(y+1)*w);
	}

	inline const Cluster & First() const {
		return Clusters.front();
	}
};

struct NukeRes {
	//GroupInfo Inf;
	int Dmg = 0;
	int Killed = 0;

	NukeRes() {

	}
	NukeRes(vec2 Point, Faction f) {
		InitNukeRes(Point, f);
	}

	bool operator<(const NukeRes & r) const {
		if (Killed == r.Killed) {
			return Dmg < r.Dmg;
		}
		return Killed < r.Killed;
	}

	void InitNukeRes(vec2 Point, Faction f) {
		Killed = 0;
		Dmg = 0;

		double NukeRadius = ThisGame.getTacticalNuclearStrikeRadius();
		VehicleGp NukeArea = VehicleGp::GetAll().FilterRadius(Point, NukeRadius).FilterFaction(f);
		//Inf = NukeArea.GetInfo();

		for (const auto & Vc : NukeArea.V) {
			double Dist = Point.dist(Vc);
			if (Dist <= NukeRadius) {
				int dmg = (int)(Dist / NukeRadius * 99);
				if (dmg >= Vc.getDurability()) {
					Killed++;
				}
				Dmg += std::min(Vc.getDurability(), dmg);
			}
		}
	}
};

Vehicle GetBestNukeUnit( vec2 Point, const VehicleGp & All ) {
	VehicleGp NukeUsers = All.FilterRadius(Point,120);
	if (NukeUsers.V.empty()) return Vehicle();
	NukeUsers.SortByDurability();
	for (const auto & Vc : NukeUsers.V) {
		if (Point.dist(Vc) > GetVisionAt(Vc)) continue;
		return Vc;
	}
	return Vehicle();
}

Rect GetSafeRect(double r) {
	return Rect(WorldMin.x + r + 1, WorldMin.y + r + 1, WorldMax.x - r - 1, WorldMax.y - r - 1);
}

class AutoSquad : public Squad {
public:

	int Id = 0;

	string DebugName;

	double Facing = 0;
	vec2 Target = Zero;

	bool CanRotate		= false;
	bool CanAttack		= false;
	bool CanFlee		= false;
	bool CanExplore		= false;
	bool CanNuke		= false;
	bool CanCapture		= false;
	bool CanPassAllies	= false;
	bool CanSpread		= false;

	double AttackDist = 120;
	double MaxSpeed = 0;
	double CapProgress = 0;
	double CapDist = 300;
	double MaxAngleSpeed = 0;

	double AttackFactor = 1.0f;

	double SpreadFactor = 1.0f;
	bool IsSpread = false;

	int ParentFactory = -1;

	bool IsInit = false;

	void LoadState() {
		SelectGroup(Id);
	}

	void Act() {
		SetNextThink(10);

		VehicleGp Group = VehicleGp::GetAll().FilterGroup(Id);

		UnitMap Enemies(128, VehicleGp::GetAll().FilterFaction(FACTION_ENEMY));
		UnitMap Self(64, Group);
		GroupInfo Inf = Group.GetInfo();

		if (!Inf.NumUnits) return;

		Enemies.SortByDist(Inf.Center);

		GroupInfo Enemy;
		double StrRatio = 0;
		if (Enemies.NumClusters > 0) {
			Enemy = Enemies.First().Inf;
		}

		StrRatio = Inf.GetStrRatio(Enemy);

		if (IsSpread) {
			StrRatio *= 0.2;
		}

		Rect SafeRect = GetSafeRect(Inf.Radius);
		vec2 VecToEnemy = Enemy.Center - Inf.Center;
		//vec2 Dir = VecToEnemy.normalise();
		double Dist = VecToEnemy.len();
		double AngleToEnemy = GetAngleTo(VecToEnemy);
		double AngleDelta = Facing - AngleToEnemy;
		vec2 FleeVec = VecToEnemy *-1;


		vector<Facility> Facilities;
		for (auto Fac : ThisWorld.getFacilities()) {
			if (Fac.getOwnerPlayerId() == Me.getId()) continue;
			//if (RectOccupied(GetFacilityRect(Fac), PRESENCE_GND,FACTION_ALLY)) continue;
			Facilities.push_back(Fac);
		}
		SortVector(Facilities, SortByDist(Inf.Center));
		Facility ClosestFac;
		if (!Facilities.empty()) ClosestFac = Facilities.front();
		Rect FacilityRect = GetFacilityRect(ClosestFac);
		vec2 VecToFacility = FacilityRect.Center() - Inf.Center;
		bool IsCapturing = true;
		IsCapturing &= (ClosestFac.getId() != -1);
		IsCapturing &= (ClosestFac.getCapturePoints() > CapProgress);
		IsCapturing &= (ClosestFac.getOwnerPlayerId() != Me.getId());
		IsCapturing &= FacilityRect.Intersect(Inf.Bounds);
		if (IsCapturing) {
			CapProgress = ClosestFac.getCapturePoints();
		}
		else {
			CapProgress = 0;
		}

		double SpeedLimit = 0;
		SpeedLimit = min(Inf.MinSpeed,MaxSpeed);



		int NukeCooldown = Me.getRemainingNuclearStrikeCooldownTicks();
		bool IsNukeSafe = Opponent.getRemainingNuclearStrikeCooldownTicks() > 80;
		vec2 EnemyNukeCenter = vec2(Opponent.getNextNuclearStrikeX(), Opponent.getNextNuclearStrikeY());
		int EnemyNukeIn = max(-1, Opponent.getNextNuclearStrikeTickIndex() - WorldTick);

		NukeRes EnemyNuke;

		vec2 NukeCenter = Enemy.Center;
		NukeRes Nuke;
		NukeRes NukeSelf;
		Vehicle NukeUnit;

		EnemyNuke.InitNukeRes(EnemyNukeCenter,FACTION_ALLY);

		Nuke.InitNukeRes(Enemy.Center, FACTION_ENEMY);
		NukeSelf.InitNukeRes(Enemy.Center, FACTION_ALLY);

		NukeUnit = GetBestNukeUnit(Enemy.Center, Group);

		if (NukeUnit.getId() == -1 || Nuke.Dmg == 0) {
			NukeCenter = VecToEnemy;
			NukeCenter.scale(Dist-Enemy.Radius*0.5);
			Nuke.InitNukeRes(NukeCenter, FACTION_ENEMY);
			NukeSelf.InitNukeRes(NukeCenter, FACTION_ALLY);

			NukeUnit = GetBestNukeUnit(NukeCenter, Group);
		}

		vector<NukeRes> Nukes;
		for (int c : Enemies.First().Cells) {
			NukeCenter = Enemies.GetCell(c).Center();
			Nukes.push_back( NukeRes(NukeCenter, FACTION_ENEMY) ) ;
		}

		UnitMap ArrvMap(32, VehicleGp::GetAll().FilterFaction(FACTION_ALLY).FilterType(VT::ARRV));

		//SortVector(ArrvMap.Clusters, SortByDist(Inf.Center));
		ArrvMap.SortByDist(Inf.Center);
		vec2 ArrvCenter = ArrvMap.First().Inf.Center;

		bool IsAttacked = true;
		IsAttacked &= Inf.HasDamaged;
		//IsAttacked &= ()

		double FleeRadius = Inf.Radius + 120;

		bool DoRotate = true;
		bool Flee = true;
		bool UseNuke = true;
		bool Attack = true;
		bool MoveAttack = true;
		bool Explore = true;
		bool Capture = true;
		bool FindArrv = true;
		bool Regroup = true;
		bool Spread = true;
		bool UnSpread = true;
		bool AvoidNuke = true;

		UseNuke &= CanNuke;
		UseNuke &= (NukeCooldown == 0);
		UseNuke &= (Enemy.NumUnits > 10);
		//UseNuke &= (StrRatio < 6);
		//UseNuke &= (NukeEnemy.Health > 1000 || NukeEnemy.NumUnits > 15);
		UseNuke &= (NukeUnit.getDurability() >= 80);
		//UseNuke &= (NukeUnit.getVisionRange() >= Dist);
		UseNuke &= (NukeUnit.getId() != -1);

		Attack &= CanAttack;
		Attack &= (Enemy.NumUnits != 0);
		Attack &= (StrRatio >= AttackFactor);
		Attack &= (Dist < AttackDist);
		//Attack &= (Inf.AvgHealth > 90 && Inf.Presence == PRESENCE_AIR);
		//Attack &= !IsSpread;
		//Attack &= !Inf.HasDamaged;

		Flee &= CanFlee;
		//Flee &= (StrRatio < 0.5 && StrRatio != 0 || Inf.AvgHealth < 40);
		Flee &= (Dist < FleeRadius);
		Flee &= !Attack;
		Flee &= SafeRect.Contains(Inf.Center);

		UseNuke &= (NukeSelf.Dmg == 0);

		DoRotate &= CanRotate;
		//CanRotate &= (Inf.NumUnits > 30);
		//DoRotate &= (StrRatio <= 8);
		DoRotate &= (Dist > Inf.Radius);
		DoRotate &= (Dist < AttackDist);
		DoRotate &= SafeRect.Contains(Inf.Center);
		DoRotate &= (abs(AngleDelta) > ToRad(30));

		Explore &= CanExplore;

		Capture &= CanCapture;
		Capture &= (Inf.Presence != PRESENCE_AIR);
		Capture &= (ClosestFac.getId() != -1);
		Capture &= (VecToFacility.len() <= CapDist);

		MoveAttack &= CanAttack;
		MoveAttack &= (Dist > AttackDist);

		FindArrv &= (Inf.Presence == PRESENCE_AIR);
		FindArrv &= (Inf.AvgHealth < 80);
		//FindArrv &= (Inf.Center.dist(ArrvCenter) < 350);
		FindArrv &= (ArrvMap.NumClusters > 0);

		Spread &= CanSpread;
		Spread &= !IsSpread;
		Spread &= !IsNukeSafe;
		Spread &= !Attack;
		Spread &= (Dist > Inf.Radius*SpreadFactor);
		//Spread &= GetSafeRect(Inf.Radius*SpreadFactor).Contains(Inf.Center);

		UnSpread &= CanSpread;
		UnSpread &= IsNukeSafe;
		//UnSpread |= (Dist < Inf.Radius);
		UnSpread |= IsAttacked;
		UnSpread &= IsSpread;

		Attack |= (NukeCooldown == 0 && Dist > Enemy.Radius && Dist < FleeRadius);
		Flee |= (IsAttacked && !Attack);
		Flee |= (UseNuke && NukeSelf.Dmg != 0);

		Regroup &= (Self.NumClusters > 1) || (Inf.Radius > 200);
		Regroup &= !IsSpread;

		AvoidNuke &= (EnemyNukeIn > 0);
		AvoidNuke &= (EnemyNuke.Dmg > 0);

		Log << "Processing group #" << Id << " " << DebugName << endl;
		Inf.PrintLog();
		Log << "Num enemy groups" << Enemies.NumClusters << endl;
		Log << "Enemy group" << endl;
		Enemy.PrintLog();
		Log << "StrRatio " << StrRatio << endl;
		//Log << "StrRatioRev " << Enemy.GetStrRatio(Nearby.GetInfo()) << endl;
		Log << "Angles " << ToDeg(Facing) << " " << ToDeg(AngleToEnemy) << " " << ToDeg(AngleDelta) << endl;
		Log << "Dist to enemy " << Dist << endl;
		Log << "Capturing: " << BTS(IsCapturing) << " " << CapProgress << endl;
		Log << "Speed limit " << SpeedLimit << endl;
		Log << "Nuke cooldown: " << NukeCooldown << endl;
		Log << "Flee radius: " << FleeRadius << endl;
		Log << "IsSpread " << BTS(IsSpread) << endl;


		//SpeedLimit = 0.05f;

		//SelectGroup(Id);
		if (AvoidNuke) {
			Scale(1.1, EnemyNukeCenter);
			Scale(0.9, EnemyNukeCenter);
			SetNextThink(0);
			Log << "	Nuke avoidance" << endl;
			return;
		}
		if (Spread) {
			Scale(SpreadFactor);
			IsSpread = true;
			SetNextThink(0);
			Log << "	SpreadIn" << endl;
			return;
		}
		if (UnSpread) {
			Scale(1.0 / SpreadFactor);
			IsSpread = false;
			SetNextThink(0);
			Log << "	SpreadOut" << endl;
			return;
		}
		if (DoRotate) {
			Facing = AngleToEnemy;
			Scale(1.03);
			Rotate(-AngleDelta, MaxAngleSpeed);
			Scale(0.97);
			SetNextThink(0);
			Log << "	Rotating" << endl;
			return;
		}
		if (UseNuke) {
			StopMove();
			TacticalNuclearStike(NukeCenter, (int)NukeUnit.getId());
			Target = NukeCenter;
			SetNextThink(0);
			Log << "	==!!!Tactical Nuclear Incoming!!!==" << endl;
			return;
		}
		if (Regroup) {
			Scale(0.1);
			Log << "	Regrouping" << endl;
			return;
		}
		if (Attack) {
			VecToEnemy.scale(20);
			MoveTo(VecToEnemy,SpeedLimit);
			Target = Enemy.Center;
			Log << "	Attacking" << endl;
			return;
		}
		if (Flee) {
			MoveTo(FleeVec,SpeedLimit);
			Target = FleeVec;
			Log << "	Fleeing" << endl;
			return;
		}
		if (FindArrv) {
			MoveTo(ArrvCenter - Inf.Center,SpeedLimit);
			Target = ArrvCenter;
			Log << "	Find ARRV" << endl;
			return;
		}
		if (Capture) {
			MoveTo(VecToFacility,SpeedLimit);
			Target = GetFacilityRect(ClosestFac).Center();
			Log << "	Capturing" << endl;
			return;
		}
		if (MoveAttack) {
			VecToEnemy.scale(40);
			MoveTo(VecToEnemy, SpeedLimit);
			Target = Enemy.Center;
			Log << "	Moving at enemy" << endl;
			return;
		}
		if (Explore) {
			Log << "	Explroing" << endl;
			return;
		}
		StopMove();
		Log << "	Do nothing" << endl;
	}

	static int iground(const imap & Sp) {
		if (Sp[0] && Sp[1] && Sp[2]) { return 1; }
		if (Sp[3] && Sp[4] && Sp[5]) { return 2; }
		if (Sp[6] && Sp[7] && Sp[8]) { return 3; }
		if (Sp[0] && Sp[3] && Sp[6]) { return 4; }
		if (Sp[1] && Sp[4] && Sp[7]) { return 5; }
		if (Sp[2] && Sp[5] && Sp[8]) { return 6; }
		return 0;
	}
	static int iaero(const imap & Sp) {
		if ((Sp[0] && Sp[1]) || (Sp[1] && Sp[2]) /*|| (Sp[0] && Sp[3])*/) { return 1; }
		if ((Sp[3] && Sp[4]) || (Sp[4] && Sp[5]) /*|| (Sp[3] && Sp[5])*/) { return 2; }
		if ((Sp[6] && Sp[7]) || (Sp[7] && Sp[8]) /*|| (Sp[6] && Sp[8])*/) { return 3; }
		if ((Sp[0] && Sp[3]) || (Sp[3] && Sp[6]) /*|| (Sp[3] && Sp[6])*/) { return 4; }
		if ((Sp[1] && Sp[4]) || (Sp[4] && Sp[7]) /*|| (Sp[1] && Sp[7])*/) { return 5; }
		if ((Sp[2] && Sp[5]) || (Sp[5] && Sp[8]) /*|| (Sp[5] && Sp[8])*/) { return 6; }
		return 0;
	}

	static vec2 iposvec(int N) {
		switch (N) {
		case 0: return vec2(P1, P1);
		case 1: return vec2(P2, P1);
		case 2: return vec2(P3, P1);
		case 3: return vec2(P1, P2);
		case 4: return vec2(P2, P2);
		case 5: return vec2(P3, P2);
		case 6: return vec2(P1, P3);
		case 7: return vec2(P2, P3);
		case 8: return vec2(P3, P3);
		}
		return vec2();
	}

	static Rect GetCell(int N) {
		return Rect(iposvec(N), 30);
	}

	void AddInitCommands(bool Aerial) {
		vec2 Dir, Dir2;
		int n1, n2, n3;
		imap Map;
		vector<imove> Moves;
		int Res;
		double Factor,VShift;
		//bool Aerial;

		if (IsInit) return;
		IsInit = true;
		SetNextThink(0);

		if (!Aerial) {
			ClearSelect(VT::TANK);
			Assign(TANKGP);
			Assign(GNDGP);

			ClearSelect(VT::IFV);
			Assign(IFVGP);
			Assign(GNDGP);

			ClearSelect(VT::ARRV);
			Assign(ARRVGP);
			Assign(GNDGP);

			Map.AddType(VT::ARRV);
			Map.AddType(VT::IFV);
			Map.AddType(VT::TANK);
		}

		if (Aerial) {
			ClearSelect(VT::HELICOPTER);
			Assign(COPTERGP);
			Assign(AIRGP);

			ClearSelect(VT::FIGHTER);
			Assign(FIGHTERGP);
			Assign(AIRGP);

			Map.AddType(VT::FIGHTER);
			Map.AddType(VT::HELICOPTER);
		}

		icmp cmp;

		if (Aerial) cmp = iaero; else cmp = iground;

		Moves.clear();
		Res = RecursiveBuild(Map, cmp, Moves);
		for (auto Im : Moves) {
			SelectGroup(Im.g);
			MoveTo(iposvec(Im.c2) - iposvec(Im.c1));
		}
		WaitForGroup(Id);

		imap Final;
		if (!Moves.empty()) {
			Final = Moves.back().m;
		}
		else {
			Final = Map;
		}

		imap::GetLine(Res, n1, n2, n3);

		int g1, g2, g3;

		g1 = Final[n1];
		g2 = Final[n2];
		g3 = Final[n3];

		if (Res <= 3) {
			Dir = vec2(1, 0);
			Dir2 = vec2(0, 1);
			Facing = 0;
		}
		else {
			Dir = vec2(0, 1);
			Dir2 = vec2(1, 0);
			Facing = PI*0.5;
		}

		if (Aerial) Factor = 1.5; else Factor = 2.05;
		if (Aerial) VShift = 3; else VShift = 2;

		SelectGroup(Id);
		ScaleAbs(Factor, GetCell(n1).Min);

		SelectGroup(g2);
		MoveTo(Dir2 * VShift * Factor);
		NextMove();
		if (!Aerial) {
			SelectGroup(g3);
			MoveTo(Dir2 * VShift * 2 * Factor);
		}

		WaitForGroup(Id);

		SelectGroup(g3);
		MoveTo(Dir * (-STARTING_DIST * Factor));
		NextMove();
		SelectGroup(g1);
		MoveTo(Dir * (STARTING_DIST * Factor));

		WaitForGroup(Id);

		Rect r1, r2;
		int w = 140;
		r1 = Rect(Dir2 * -w, Dir * w + Dir2 * w);
		r2 = Rect(Dir * -w + Dir2 * -w, Dir2 * w);

		double MaxSpeed;
		if (Aerial) MaxSpeed = ThisGame.getHelicopterSpeed(); else MaxSpeed = ThisGame.getTankSpeed();

		SelectLocal(r1);
		if (Aerial) Deselect(GNDGP); else Deselect(AIRGP);
		MoveTo(Dir*-50,MaxSpeed);
		SelectLocal(r2);
		if (Aerial) Deselect(GNDGP); else Deselect(AIRGP);
		MoveTo(Dir * 50,MaxSpeed);

		WaitForGroup(Id);
	}


	void Think() {
		Act();
	}
};

class Supervisor : public Squad {
	int SpawnIndex = GPSPAWNED;

	void Think() {

		vector<VehicleType> Produce;
		//Produce.push_back(VT::ARRV);
		Produce.push_back(VT::IFV);
		//Produce.push_back(VT::FIGHTER);
		Produce.push_back(VT::HELICOPTER);
		//Produce.push_back(VT::TANK);

		for (const auto & Fac : ThisWorld.getFacilities()) {
			if (Fac.getType() != FacilityType::VEHICLE_FACTORY) continue;
			if (Fac.getOwnerPlayerId() != Me.getId()) continue;
			if (Fac.getVehicleType() != VT::_UNKNOWN_) continue;
			if (RectOccupied(GetFacilityRect(Fac), PRESENCE_GND,FACTION_ALLY)) continue;

			SetupProduction(Produce[rand() % Produce.size()], ID(Fac));
		}

		if (WorldTick < 100) return;

		UnitMap Allies;
		Allies.Init(32, VehicleGp::GetAll().FilterFaction(FACTION_ALLY) );

		for (UnitMap::Cluster & C : Allies.Clusters) {
			VehicleGp & Gp = C.Gp;

			if (!Gp.GetInfo().Groups.empty()) continue;
			if (Gp.V.size() < 40) continue;

			Gp.GetInfo().PrintLog();

			AutoSquad * Sq = new AutoSquad();
			Sq->Id = SpawnIndex++;
			Sq->IsInit = true;
			Sq->CanAttack = true;
			Sq->CanFlee = true;
			Sq->CanNuke = true;
			Sq->CanSpread = true;
			Sq->AttackDist = 100;
			Sq->CanCapture = true;
			Sq->CapDist = 200;
			Sq->ClearSelect(Gp.GetInfo().Bounds);
			Sq->Assign(Sq->Id);
			Sq->SetNextThink(0);
			//Sq->ParentFactory = Fac.getId();
			Squad::AddSquad(Sq);
		}
	}
};

void InitSquads() {
	if (WorldTick != 0) return;

	AutoSquad * AGp = new AutoSquad();
	AutoSquad * Gp = new AutoSquad();

	AGp->Id = AIRGP;
	AGp->CanRotate = true;
	AGp->CanExplore = false;
	AGp->CanAttack = true;
	AGp->CanFlee = true;
	AGp->CanNuke = true;
	AGp->MaxSpeed = VhStats(VT::HELICOPTER).Speed * 0.8;
	AGp->AttackDist = 180;
	AGp->MaxAngleSpeed = 0.03;
	AGp->CanSpread = true;
	AGp->SpreadFactor = 2.2f;
	//AGp->AttackFactor = 2.5f;
	//AGp->Can

	AGp->AddInitCommands(true);

	AGp->DebugName = "Aerial";

	Gp->Id = GNDGP;
	Gp->CanRotate = true;
	Gp->CanExplore = false;
	Gp->CanCapture = true;
	Gp->CanAttack = true;
	Gp->CanFlee = true;
	Gp->CanNuke = true;
	Gp->CapDist = 350;
	Gp->AttackDist = 200;
	Gp->MaxSpeed = VhStats(VT::TANK).Speed * 0.7;
	Gp->MaxAngleSpeed = 0.01;
	Gp->CanSpread = true;
	Gp->SpreadFactor = 1.7;

	Gp->AddInitCommands(false);

	Gp->DebugName = "Ground";

	Squad::AddSquad(AGp);
	Squad::AddSquad(Gp);
	Squad::AddSquad(new Supervisor());
}


void TestAngles() {
	Log << ToDeg(vec2(100,0).angle(vec2(100000, 0))) << " ";
	Log << ToDeg(vec2(100, 0).angle(vec2(0, -10000))) << " ";
	Log << ToDeg(vec2(100, 0).angle(vec2(-1000, 0))) << " ";
	Log << ToDeg(vec2(100, 0).angle(vec2(0, 10))) << " " << endl;
}

void TestCluster() {
	Log << "Cluster" << endl;
	UnitMap Map;
	//Map.Init(128,FACTION_ENEMY);
	int n = 0;
	Log << Map.NumClusters << endl;
	//for (auto & Gp : Map.Groups) {
	//	Gp.GetInfo().PrintLog();
	//}
	Log << endl;
}

void TestStrRatio() {
	GroupInfo Inf1, Inf2;

	Inf1.Health = 200 * 100;
	Inf1.NumType[(int)VT::HELICOPTER] = 100;
	Inf1.NumType[(int)VT::FIGHTER] = 100;

	Inf2.Health = 100 * 100;
	Inf2.NumType[(int)VT::IFV] = 100;

	double Str = Inf1.GetStrRatio(Inf2);
	Log << Str << endl;
}

void RunTests() {
	TestAngles();
	TestStrRatio();
	TestCluster();
	exit(0);
}

class CoroutineTest : public Squad {
public:
	int id = 0;
	bool init = false;
	CoroutineTest(int _id) {
		id = _id;
	}
	void Think() {
		if (!init) {
			ClearSelect((VT)id);
			Assign(id+1);
			init = true;
		}
		else {
			MoveTo(vec2(10, 0));
		}
	}
};

void InitGame() {
	if (WorldTick != 0) return;

	//Squads.push_back(new CoroutineTest(0));
	//Squads.push_back(new CoroutineTest(1));
	//Squads.push_back(new CoroutineTest(2));
	//RunTests();
	InitSquads();
}

void MyStrategy::move(const Player& me, const World& world, const Game& game, Move & move) {
	InitGlobals(me, world, game);
	InitUnits();
	if (Me.getRemainingActionCooldownTicks() > 0) return;
	//Log << "Actions left: " << Me.get << endl;
	InitGame();

	//int n = 0;
	//while (true) {
	//	n++;
	//	move = Squad::GetNextMove();
	//	if (n > Squads.size()) break;
	//	if (move.getAction() == AT::_UNKNOWN_) continue;
	//	if (move.getAction() == AT::NONE) continue;
	//	break;
	//}

	move = Squad::GetNextMove();
}

//void TestRecursive() {
//	Log << "RECURSIVE" << endl;
//	imap Sp;
//	Sp[3] = 1;
//	Sp[5] = 2;
//	Sp[6] = 3;
//
//	vector<imove> V;
//	int N = 2;
//	int Res = RecursiveBuild(Sp, &iground, V);
//	Log << Res << endl;
//	//RecursiveBuild(Sp, 4, &iground, V);
//	for (auto M : V) {
//		Log << M.c1 << " " << M.c2 << " " << M.g << endl;
//	}
//	Log << endl;
//	exit(0);
//}



MyStrategy::MyStrategy() {
#ifdef DEBUG
	Log.open("Log.txt");
#endif
}
#ifdef DEBUG
MyStrategy::~MyStrategy() {
	Log.close();
}
#endif
